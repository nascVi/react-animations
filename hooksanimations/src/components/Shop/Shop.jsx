import React, { useState, useEffect } from 'react';

const Shop = props => {

  const [cart, setCart] = useState([])
  const cartHandler = () => {
  	setCart(['A Book'])
  }

  const [products, setProducts] = useState([])

  useEffect(() => {
  fetch('my-backend.com/products')
  .then(res => res.json())
  .then(fetchedProducts => setProducts(fetchedProducts))
  })

	return (
	  <div>
	    <h2>Animated Shop</h2>
	    <button onClick={cartHandler}>
	    	Add to Cart
	    </button>
	    <br />
	    <ul>
	    	{products.map(product => (
	    		<li key={product.id}>
	    		{product.name}
	    		</li>
	    	))}
	    </ul>
	    <br />
	  </div>
	);
};

export default Shop;
