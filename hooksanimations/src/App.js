import React from 'react';
import Shop from "./components/Shop/Shop";
import './App.css';

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Animated React</h1>  
      </header>
      <Shop />
    </div>
  );
}

export default App;
